import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  loadedFeauture = 'recipe';

  public onNavigate(feature: string) {
    this.loadedFeauture = feature;
  }
}
