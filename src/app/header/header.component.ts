import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})

export class HeaderComponent {
  public collapsed = true;
  @Output() feautureSelected = new EventEmitter<string>();

  public onSelect(feature: string) {
    this.feautureSelected.emit(feature);
  }
}
